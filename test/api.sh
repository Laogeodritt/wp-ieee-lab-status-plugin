#! /usr/bin/env bash

# Usage: ./api.sh http://test-site.example.com/labstat
# You can append additional curl arguments as well.
# You need to set up users in Wordpress first:
#    test-labstat : password "asdf", role "Lab Status Client"
#    test-other : password "qwer", role "Subscriber"

CURL_ARGS=$@

function labstat-write() {
    curl -d"labstat-value=$1&log=test-labstat&pwd=asdf&rememberme=false" -k $CURL_ARGS
    echo
}

function labstat-read() {
    curl -k $CURL_ARGS
    echo
}

function labstat-autherr() {
    curl -d"labstat-value=$1&log=test-labstat&pwd=thisiswrong&rememberme=false" -k $CURL_ARGS
    echo
}

function labstat-roleerr() {
    curl -d"labstat-value=$1&log=test-other&pwd=qwer&rememberme=false" -k $CURL_ARGS
    echo
}
echo -e "Labstat Tester\n"

echo "Expect: open"
labstat-write open
labstat-read
echo

echo "Expect: closed"
labstat-write closed
labstat-read
echo

echo "Expect: closed (again)"
labstat-write closed
labstat-read
echo

echo "Expect: open"
labstat-write open
labstat-read
echo

echo "Expect: open (again)"
labstat-write open
labstat-read
echo

echo "Expect: Invalid value, open"
labstat-write invalid
labstat-read
echo

echo "Expect: Authentication failure, open"
labstat-autherr closed
labstat-read
echo

echo "Expect: Authentication failure (again: testing also with invalid value), open"
labstat-autherr invalid
labstat-read
echo

echo "Expect: User not permitted, open"
labstat-roleerr closed
labstat-read
echo

echo "Expect: User not permitted (again: testing also with invalid value), open"
labstat-roleerr invalid
labstat-read
echo

echo "Expect: closed"
labstat-write closed
labstat-read
echo

echo "Expect: open"
labstat-write open
labstat-read
echo

echo "END TEST. This test is not automatically verified: you should check the results match expectations."
echo

