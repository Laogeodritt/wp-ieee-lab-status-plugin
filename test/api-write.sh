#! /usr/bin/env bash

# Manually write to the API.
# Usage: ./api-write.sh <value>  http://test-site.example.com/labstat
# You can append additional curl arguments as well.
# You need to set up users in Wordpress first:
#    test-labstat : password "asdf", role "Lab Status Client"
#    test-other : password "qwer", role "Subscriber"

value=$1
CURL_ARGS=${@:2}

function labstat-write() {
    curl -d"labstat-value=$1&log=test-labstat&pwd=asdf&rememberme=false" -k $CURL_ARGS
    echo
}

function labstat-read() {
    curl -k $CURL_ARGS
    echo
}

labstat-write $value
labstat-read

