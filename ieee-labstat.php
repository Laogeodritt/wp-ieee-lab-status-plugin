<?php

/*
Plugin Name: WP IEEE Labstat
Plugin URI: http://arenthil.net/portfolio/wp-ieee-labstat
Description: Developed for IEEE Concordia. Allows displaying whether or not the lab is open to visitors. Implements a basic REST API for an external device to report current lab status (open/closed), as well as a widget and shortcode for displaying this information on the page.
Version: 1.0
Author: Marc-Alexandre Chan <marcalexc@arenthil.net>
Author URI: http://arenthil.net
License: GPLv3
*/

/*
 * == USER ROLES ==
 *
 * This plugin adds a "Lab Status API" role. This role is equivalent to the Subscriber role, with
 * the additional capability of being able to update the lab status via this plugin's API.
 *
 * Users under this role should be used exclusively by API clients. This is to mitigate the risk of
 * account compromise: clients are expected to be publicly accessible hardware devices or computer
 * terminals, in a publicly accessible supervised space. A client may choose to save a login cookie,
 * a cleartext password or both in order to ensure minimum maintenance of the lab status
 *
 * == API REFERENCE ==
 *
 * All requests must be made to /labstat/
 *
 * A GET request will return the lab status. No authentication is required. No parameters.
 *
 * A POST request will update the lab status. Authentication is required.
 * Parameters:
 *      labstat-value  string  A valid value for lab status. One of 'closed', 'open'.
 *          See also $ieee_labstat_values.
 *      log  string  Username for login. Valid user must have the 'Lab Status' role.
 *      pwd  string  Password for login.
 *
 */

add_filter('query_vars', 'ieee_labstat_query_vars');

add_action('init', 'ieee_labstat_setup_rewrite');
add_action('parse_request', 'ieee_labstat_parse_request');
add_action('widgets_init', 'ieee_labstat_register_widgets');

add_shortcode('labstat', 'ieee_labstat_shortcode');

register_activation_hook(__FILE__, 'ieee_labstat_activate');
register_deactivation_hook(__FILE__, 'ieee_labstat_deactivate');


/*************************************************************************************************
 * INITIALIZATION
 *************************************************************************************************/
/**
 * Because globals are scary and PHP constants are weird. I know this is terrible.
 *
 * @return object
 */
function ieee_labstat_get_config()
{
    $cfg = (object) array(
        'values'         => array( 'closed', 'open', 'scheduled', 'busy' ), // valid status values
        'values_display' => array( // display/translatable strings: needs to match 'values' array
            __('Closed', 'ieee-labstat'),
            __('Open', 'ieee-labstat'),
            _x('Scheduled Open',
                'Lab schedule indicates it is supposed to be open, but nobody has explicitly opened it.',
                'ieee-labstat'),
            _x('Busy', 'Lab schedule indicates lab is busy.', 'ieee-labstat')
        ),
        'option'         => 'ieee-labstat-value', // wordpress option name for status
        'json_value_key' => 'labstat-value', // JSON key for the returned value in responses
        'api_name'       => 'labstat', // component of URL to access JSON API
        'role'           => (object) array(
            'id'           => 'ieee-labstat',
            'name'         => 'Lab Status Client',
            'capabilities' => array( 'read' => true )
        ),
        'widget'         => (object) array(
            'id'                   => 'ieee_labstat_widget',
            'name'                 => 'Lab Status Widget',
            'desc'                 => 'Labstat plugin: shows current status of the lab/space.',
            'css_class'            => 'ieee-labstat-widget',
            'css_body_class'       => 'ieee-labstat-body',
            'css_state_class_base' => 'ieee-labstat-'
        ),
    );

    return $cfg;
}

/**
 * Sets up the rewrite for the /labstat URL into an index.php query var.
 */
function ieee_labstat_setup_rewrite()
{
    $cfg = ieee_labstat_get_config();
    add_rewrite_rule("{$cfg->api_name}/?$", "index.php?{$cfg->api_name}", 'top');
}

/**
 * query_vars filter. Adds 'labstat' as a query var.
 *
 * @param array $vars Input query vars from Wordpress.
 *
 * @return array Processed query vars.
 */
function ieee_labstat_query_vars($vars)
{
    $cfg    = ieee_labstat_get_config();
    $vars[] = $cfg->api_name;

    return $vars;
}

/**
 * Actions to take when activating the plugin in the Admin panel. Rewrites are set up and a default
 * value for the lab status is set. The Lab Status user role is added.
 */
function ieee_labstat_activate()
{
    $cfg = ieee_labstat_get_config();

    ieee_labstat_setup_rewrite();
    flush_rewrite_rules();

    add_option($cfg->option, $cfg->values[0]);
    add_role($cfg->role->id, __($cfg->role->name, 'ieee_labstat'), $cfg->role->capabilities);
}

/**
 * Actions to take when deactivating the plugin in the Admin panel.
 */
function ieee_labstat_deactivate()
{
    flush_rewrite_rules();

    // commented for now - avoid losing state on deactivate
    //$cfg = ieee_labstat_get_config();
    //delete_option($cfg->option);
    //remove_role($cfg->role->id);
}


/*************************************************************************************************
 * API
 *************************************************************************************************/

/**
 * Returns the current status value. If the current status value is invalid, this method has the
 * side effect of setting it to the first valid value defined in config->values.
 *
 * @return string|bool Current status value.
 */
function ieee_labstat_get_value()
{
    $cfg    = ieee_labstat_get_config();
    $status = get_option($cfg->option, $cfg->values[0]);

    // If the retrieved value is invalid, let's default to "closed"
    if( !in_array($status, $cfg->values) )
    {
        update_option($cfg->option, $cfg->values[0]);
        $status = $cfg->values[0];
    }

    return $status;
}

/**
 * Parse all requests. If the request is destined for this plugin's API, process the request.
 *
 * @param WP $wp A WP object from the parse_request wordpress action
 */
function ieee_labstat_parse_request($wp)
{
    $cfg = ieee_labstat_get_config();
    if( array_key_exists($cfg->api_name, $wp->query_vars) )
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch($method)
        {
            case 'GET':
                ieee_labstat_api_get();
                break;
            case 'POST':
                ieee_labstat_api_post();
                break;
            default:
                wp_send_json_error(array( 'msg' => 'Unsupported HTTP method \'' . $method . '\'' ));
        }
    }
}

/**
 * Process a GET request on the plugin JSON interface. Send the current lab status value via
 * JSON. If the value in the database is invalid (e.g. due to an up/downgrade of the plugin),
 * the value is silently reset to the first value defined in $ieee_labstat_values.
 */
function ieee_labstat_api_get()
{
    $cfg = ieee_labstat_get_config();
    wp_send_json_success(array( $cfg->json_value_key => ieee_labstat_get_value() ));
}

/**
 * Process a POST request on the plugin JSON interface. Checks if the user is allowed to POST
 * and whether the status value defined is valid. Stores the value, of so. Sends JSON success
 * or error.
 */
function ieee_labstat_api_post()
{
    $cfg = ieee_labstat_get_config();

    if( !ieee_labstat_can_user_write(wp_get_current_user()) )
    {
        $user = wp_signon(); // let this use $_POST parameters

        if( is_wp_error($user) )
        {
            wp_send_json_error(array( 'msg' => 'Authentication failure' ));
        }
        elseif( !ieee_labstat_can_user_write($user) )
        {
            wp_send_json_error(array( 'msg' => 'Access denied: user not permitted' ));
        }
    }

    // validate the new value
    if( !array_key_exists($cfg->json_value_key, $_POST) )
    {
        wp_send_json_error(array( 'msg' => "Missing parameter '{$cfg->json_value_key}'" ));
    }

    $new_status = $_POST[ $cfg->json_value_key ];

    if( !in_array($new_status, $cfg->values) )
    {
        wp_send_json_error(array( 'msg' => "Invalid value for parameter '{$cfg->json_value_key}'" ));
    }

    update_option($cfg->option, $new_status);
    wp_send_json_success(array( $cfg->json_value_key => $new_status ));
}

/**
 * Check whether the passed user is allowed to execute write operations via the labstat API.
 * You can pass the result of wp_get_current_user() or wp_signon() to this method.
 *
 * @param $user WP_User of the currently logged in user
 * @return bool
 */
function ieee_labstat_can_user_write($user)
{
    $cfg = ieee_labstat_get_config();

    return ($user !== false) && !is_wp_error($user) && in_array($cfg->role->id, $user->roles);
}


/*************************************************************************************************
 * WIDGET
 *************************************************************************************************/

function ieee_labstat_register_widgets()
{
    register_widget('IeeeLabstatWidget');
}

class IeeeLabstatWidget extends WP_Widget {
    private $valid_states;
    private $valid_states_display;
    private $cfg;

    public function __construct()
    {
        $this->valid_states         =& ieee_labstat_get_config()->values;
        $this->valid_states_display =& ieee_labstat_get_config()->values_display;
        $this->cfg                  =& ieee_labstat_get_config()->widget;

        $i8n_name    = __($this->cfg->name, 'ieee-labstat');
        $widget_ops  = array(
            'classname'   => $this->cfg->css_class,
            'description' => __($this->cfg->desc, 'ieee-labstat')
        );
        $control_ops = array( 'id_base' => $this->cfg->id );

        parent::__construct($this->cfg->id, $i8n_name, $widget_ops, $control_ops);
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args Display arguments including before_title, after_title, before_widget, and
     *     after_widget.
     * @param array $instance Settings from the database for the particular instance of the widget.
     */
    public function widget($args, $instance)
    {
        $status = ieee_labstat_get_value();

        $has_title = !empty($instance['title']);
        if( $has_title )
        {
            $filt_title =
                apply_filters('widget_title', $instance['title'], $instance, $this->id_base);
        }
        else
        {
            $filt_title = '';
        }

        $has_text = !empty($instance[ $status ]);
        if( $has_text )
        {
            $filt_text = apply_filters('widget_text', $instance[ $status ], $instance);

            if( !empty($instance['filter']) )
            {
                $filt_text = wpautop($filt_text);
            }
        }
        else
        {
            $filt_text = '';
        }

        $classes = array( $this->cfg->css_body_class, $this->cfg->css_state_class_base + $status );

        echo $args['before_widget'];
        if( $has_title )
        {
            echo $args['before_title'] . $filt_title . $args['after_title'];
        }
        ?>
        <div class="<?php echo implode(' ', $classes); ?>"><?php echo $filt_text; ?></div>
        <?php
        echo $args['after_widget'];
    }

    /**
     * Outputs the settings update form on the admin panel.
     *
     * @param array $instance Current settings from the database.
     * @return string|null Default return is 'noform'.
     */
    public function form($instance)
    {
        $instance   = wp_parse_args((array) $instance, $this->get_default_config());
        $title      = strip_tags($instance['title']);
        $state_text = array();
        foreach( $this->valid_states as $state )
        {
            $state_text[ $state ] = esc_textarea($instance[ $state ]);
        }
        $state_label = __('Content for "%s":', 'ieee-labstat');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>

        <?php foreach( $this->valid_states as $i => $state ): ?>
        <p><label for="<?php echo $this->get_field_id($state); ?>"><?php printf($state_label,
                    $this->valid_states_display[ $i ]); ?></label>
            <textarea class="widefat" rows="8" cols="20"
                      id="<?php echo $this->get_field_id($state); ?>"
                      name="<?php echo $this->get_field_name($state); ?>"><?php echo $state_text[ $state ]; ?></textarea>
        </p>
    <?php endforeach; ?>

        <p><input id="<?php echo $this->get_field_id('filter'); ?>"
                  name="<?php echo $this->get_field_name('filter'); ?>"
                  type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] :
                0); ?> />&nbsp;<label
                for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs'); ?></label>
        </p>
        <?php
    }

    /**
     * Return the default configuration of the widget (for use in the form, etc.).
     *
     * @return array
     */
    private function get_default_config()
    {
        $default          = array();
        $default['title'] = '';
        foreach( $this->valid_states as $i => $state )
        {
            $default[ $state ] =
                '<p style="font-size: 150%; font-weight: bold; text-align: center;">'
                . $this->valid_states_display[ $i ] . '</p>';
        }
        $default['filter'] = false;

        return $default;
    }

    /**
     * Update a particular widget instance.
     *
     * This function should check that $new_instance is set correctly. The newly-calculated value
     * of $instance should be returned. If false is returned, the instance won’t be saved/updated.
     *
     * @param array $new_instance The new options.
     * @param array $old_instance The previous options.
     * @return string The validated/sanitised $new_instance.
     */
    public function update($new_instance, $old_instance)
    {
        $instance          = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        if( current_user_can('unfiltered_html') )
        {
            foreach( $this->valid_states as $state )
            {
                $instance[ $state ] = $new_instance[ $state ];
            }
        }
        else
        {
            foreach( $this->valid_states as $state )
            {
                // apparently wp_filter_post_kses expects slashes?
                // see WP_Widget_Text in default-widgets.php ... (version 4.3.1)
                $instance[ $state ] =
                    stripslashes(wp_filter_post_kses(addslashes($new_instance[ $state ])));
            }
        }
        $instance['filter'] = !empty($new_instance['filter']);

        return $instance;
    }
}


/*************************************************************************************************
 * SHORTCODES
 *************************************************************************************************/

/**
 * Shortcode handler for the plugin's conditional shortcode.
 *
 * Example usage:
 *
 * [labstat value="open"]
 * <h3>Open</h3>
 * <p>This will show up when the lab is open.</p>
 * [/labstat]
 * [labstat value="closed"]
 * <h3>Closed</h3>
 * <p>This will show up when the lab is closed.</p>
 * [/labstat]
 *
 * @param array $atts Shortcode attributes passed in the tag
 * @param string $content Shortcode tag contents
 * @return string HTML to display in rendered post.
 */
function ieee_labstat_shortcode($atts, $content)
{
    $cfg_values = ieee_labstat_get_config()->values;
    $status     = ieee_labstat_get_value();
    $tag_status = isset($atts['value']) ? strtolower($atts['value']) : '';

    // Only show the content if the tag matches
    if( !empty($tag_status) && in_array($tag_status, $cfg_values) && $tag_status === $status )
    {
        return $content;
    }
    else
    {
        return '';
    }
}
